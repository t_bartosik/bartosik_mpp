Versatile Calculator (CZ)
=========================

*  Kalkulačka (*Calculator*) nabízí možnost zadání textového řetězce číslic oddělených operátory **+**, **-**, ***** a **/** se zobrazením výsledku a **historie operací**.
*  Převodník číselných soustav (*Converter*) umožní zadaný číselný řetězec převádět mezi **binární**, **osmičkovou**, **desítkovou** a **šestnáctkovou** číselnou soustavou, rovněž s volitelným **zobrazením historie**.
*  Kodér (*Encoder*) využívá interpretace **Shannon-Fanovy** metody kódování znaků dle jejich frekvence užití s následným výpisem kódovací tabulky a výsledného výstupu s okamžitou zpětnou konverzí. Funkcionalita je rozšířena o možnost **dodatečného výpisu** informací o kódování, **načítání vstupu** a **ukládání výstupu do uživatelem definovaného souboru**.
*  Aplikace disponuje možností **vytvoření snímku obrazovky** aktuálně zobrazeného modulu uživatelského rozhraní.
*  Je obsaženo okno s logem a informacemi o názvu a verzi aplikace společně s **odkazem do tohoto repozitáře**.
*  Implementovány dva přepínatelné jazyky: **Čeština** a **Angličtina**.
*  **Responzivní uspořádání prvků** uživatelského rozhraní s nastavením limitů velikosti oken.
*  Sada **jednotkových testů** jakožto samostatný projekt pro ověření funkcionality implementovaných součástí aplikace.
*  **Vnitřní dokumentace zdrojového kódu** (členských funkcí) ve stylu generátoru *Doxygen*.




Versatile Calculator (EN)
=========================

*  *Calculator* offers the possibility of processing basic mathematic operations using **+**, **-**, ***** and **/** operators with displaying current results and **history of operations**.
*  *Converter* allows the conversion of numbers defined in input string between **binary**, **octal**, **decimal** and **hexadecimal** number bases with additional **history of operations**.
*  The *Encoder* module implements an interpretation of **Shannon-Fano** coding method that is used for coding input characters based on their frequency of occurrence, providing the output with backward decoding. The application is expanded by **additional informations** concerning the conversion, possibility of **loading input** and **saving output to a user-defined file**.
*  The application offers the possibility of **capturing a screenshot** of currently active window.
*  A pop-up window containing basic informations involving logo, name, version and a **link to this repository** is also implemented.
*  The application provides a selection between two languages: **Czech** and **English**.
*  The user interface is handled in a **responsive manner** depending on current size of the window, which is also limited.
*  Additional **unit test suites** are implemented as a standalone project to verify the functionality of modules.
*  The source code (member functions) is covered by a *Doxygen* themed **documentation**.