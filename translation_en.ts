<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="20"/>
        <source>Dialog</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="about.ui" line="85"/>
        <source>Versatile Calculator</source>
        <translation>Versatile Calculator</translation>
    </message>
    <message>
        <location filename="about.ui" line="146"/>
        <source>Visit repository</source>
        <translation>Visit repository</translation>
    </message>
    <message>
        <location filename="about.ui" line="153"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>Calculator</name>
    <message>
        <location filename="calculator.cpp" line="109"/>
        <location filename="calculator.cpp" line="118"/>
        <source>Unsupported format.</source>
        <translation>Unsupported format.</translation>
    </message>
</context>
<context>
    <name>Converter</name>
    <message>
        <location filename="converter.cpp" line="273"/>
        <source>Unsupported format.</source>
        <translation>Unsupported format.</translation>
    </message>
</context>
<context>
    <name>Detail</name>
    <message>
        <location filename="detail.ui" line="26"/>
        <source>Dialog</source>
        <translation>Encoding details</translation>
    </message>
    <message>
        <location filename="detail.ui" line="82"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="detail.cpp" line="40"/>
        <source>Conversion</source>
        <translation>Conversion</translation>
    </message>
    <message>
        <location filename="detail.cpp" line="40"/>
        <source>Occurrences</source>
        <translation>Occurrences</translation>
    </message>
</context>
<context>
    <name>Encoder</name>
    <message>
        <location filename="encoder.cpp" line="148"/>
        <source>Unsupported format.</source>
        <translation>Unsupported format.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation>Versatile Calculator</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="35"/>
        <source>Encode</source>
        <translation>Encode</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="51"/>
        <source>Load from file</source>
        <translation>Load input file</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Save to file</source>
        <translation>Save output file</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>Show details</source>
        <translation>Show details</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="116"/>
        <source>Calculate</source>
        <translation>Calculate</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="139"/>
        <location filename="mainwindow.ui" line="265"/>
        <source>Show history</source>
        <translation>Show history</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="196"/>
        <location filename="mainwindow.ui" line="224"/>
        <source>Binary</source>
        <translation>Binary</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="mainwindow.ui" line="229"/>
        <source>Octal</source>
        <translation>Octal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="mainwindow.ui" line="234"/>
        <source>Decimal</source>
        <translation>Decimal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <location filename="mainwindow.ui" line="239"/>
        <source>Hexadecimal</source>
        <translation>Hexadecimal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="mainwindow.ui" line="335"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="mainwindow.ui" line="346"/>
        <source>Calculator</source>
        <translation>Calculator</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="354"/>
        <location filename="mainwindow.ui" line="357"/>
        <source>Converter</source>
        <translation>Converter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <location filename="mainwindow.ui" line="368"/>
        <source>Encoder</source>
        <translation>Encoder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <source>Capture screenshot</source>
        <translation>Capture screenshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <location filename="mainwindow.cpp" line="260"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="386"/>
        <source>Czech</source>
        <translation>Czech</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <source>Open text file</source>
        <translation>Open text file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Text file (*.txt)</source>
        <translation>Text file (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <source>Loading error</source>
        <translation>Loading error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <source>File could not be opened.</source>
        <translation>File could not be opened.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Save text file</source>
        <translation>Save text file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="215"/>
        <source>output.txt</source>
        <translation>output.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <location filename="mainwindow.cpp" line="248"/>
        <source>Saving error</source>
        <translation>Saving error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <location filename="mainwindow.cpp" line="248"/>
        <source>Unable to save file.</source>
        <translation>Unable to save file.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Detail</source>
        <translation>Encoding details</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="245"/>
        <source>Save screenshot</source>
        <translation>Save screenshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="245"/>
        <source>screenshot.png</source>
        <translation>screenshot.png</translation>
    </message>
</context>
</TS>
