QT += testlib
QT += core gui widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

HEADERS += ../calculator.hpp  \
                   ../converter.hpp  \
                   ../encoder.hpp  \

SOURCES +=  ver_calc_tests.cpp  \
                    ../calculator.cpp  \
                    ../converter.cpp  \
                    ../encoder.cpp  \
