/**
 * @file        ver_calc_tests.cpp
 * @author      Tomas Bartosik
 * @date        15.12.2020
 * @brief       tests definition file for Versatile Calculator
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include <QtTest>
#include <QCoreApplication>
#include "../calculator.hpp"
#include "../converter.hpp"
#include "../encoder.hpp"

/*Class definition: ---------------------------------------------------------*/
class VerCalcTest : public QObject
{
    Q_OBJECT

public:
    VerCalcTest();
    ~VerCalcTest();

private slots:
    void calcSumTest();
    void calcSubstractTest();
    void calcMultiplyTest();
    void calcDivideTest();
    void calcDecimalsTest();
    void calcMultiTest();
    void calcUnsupportedTest();
    void convFromBinaryTest();
    void convFromOctalTest();
    void convFromDecimalTest();
    void convFromHexadecimalTest();
    void codeFirstGoodString();
    void codeSecondGoodString();
    void codeThirdGoodString();
};

VerCalcTest::VerCalcTest(){}

VerCalcTest::~VerCalcTest(){}

/*Class Calculator tests: ---------------------------------------------------*/
void VerCalcTest::calcSumTest()
{
    Calculator *calc = new Calculator();
    QString sumString = "4+6+72";
    QVERIFY(calc->Calculator_Parse(sumString) == "82");
    delete calc;
}

void VerCalcTest::calcSubstractTest()
{
    Calculator *calc = new Calculator();
    QString subsString = "9-69-27";
    QVERIFY(calc->Calculator_Parse(subsString) == "-87");
    delete calc;
}

void VerCalcTest::calcMultiplyTest()
{
    Calculator *calc = new Calculator();
    QString multiplyString = "8*9*3*1";
    QVERIFY(calc->Calculator_Parse(multiplyString) == "216");
    delete calc;
}

void VerCalcTest::calcDivideTest()
{
    Calculator *calc = new Calculator();
    QString divideString = "166/8";
    QVERIFY(calc->Calculator_Parse(divideString) == "20.75");
    delete calc;
}

void VerCalcTest::calcDecimalsTest()
{
    Calculator *calc = new Calculator();
    QString decimalsString = "0.67 + 4,14 - 955.6";
    QVERIFY(calc->Calculator_Parse(decimalsString) == "-950.79");
    delete calc;
}

void VerCalcTest::calcMultiTest()
{
    Calculator *calc = new Calculator();
    QString multiString = "8+16,4*2-90/3";
    QVERIFY(calc->Calculator_Parse(multiString) == "10.8");
    delete calc;
}

void VerCalcTest::calcUnsupportedTest()
{
    Calculator *calc = new Calculator();
    QString incorrectString = "jhad ffsafasf";
    QVERIFY(calc->Calculator_Parse(incorrectString) == "Unsupported format.");
    delete calc;
}

/*Class Converter tests: ----------------------------------------------------*/
void VerCalcTest::convFromBinaryTest()
{
    Converter *conv = new Converter();
    QString binaryInputString = "011010011101";
    QString badBinaryInputString = "01231101";
    QVERIFY(conv->Converter_Parse(binaryInputString, BINARY, BINARY) == binaryInputString);
    QVERIFY(conv->Converter_Parse(binaryInputString, BINARY, OCTAL) == "3235");
    QVERIFY(conv->Converter_Parse(binaryInputString, BINARY, DECIMAL) == "1693");
    QVERIFY(conv->Converter_Parse(binaryInputString, BINARY, HEXADECIMAL) == "69D");
    QVERIFY(conv->Converter_Parse(badBinaryInputString, BINARY, BINARY) == "Unsupported format.");
    delete conv;
}

void VerCalcTest::convFromOctalTest()
{
    Converter *conv = new Converter();
    QString octalInputString = "621";
    QString badOctalInputString = "2941";
    QVERIFY(conv->Converter_Parse(octalInputString, OCTAL, BINARY) == "110010001");
    QVERIFY(conv->Converter_Parse(octalInputString, OCTAL, OCTAL) == octalInputString);
    QVERIFY(conv->Converter_Parse(octalInputString, OCTAL, DECIMAL) == "401");
    QVERIFY(conv->Converter_Parse(octalInputString, OCTAL, HEXADECIMAL) == "191");
    QVERIFY(conv->Converter_Parse(badOctalInputString, OCTAL, OCTAL) == "Unsupported format.");
    delete conv;
}

void VerCalcTest::convFromDecimalTest()
{
    Converter *conv = new Converter();
    QString decimalInputString = "657128";
    QString badDecimalInputString = "aedx552263";
    QVERIFY(conv->Converter_Parse(decimalInputString, DECIMAL, BINARY) == "10100000011011101000");
    QVERIFY(conv->Converter_Parse(decimalInputString, DECIMAL, OCTAL) == "2403350");
    QVERIFY(conv->Converter_Parse(decimalInputString, DECIMAL, DECIMAL) == decimalInputString);
    QVERIFY(conv->Converter_Parse(decimalInputString, DECIMAL, HEXADECIMAL) == "A06E8");
    QVERIFY(conv->Converter_Parse(badDecimalInputString, DECIMAL, DECIMAL) == "Unsupported format.");
    delete conv;
}

void VerCalcTest::convFromHexadecimalTest()
{
    Converter *conv = new Converter();
    QString hexadecimalInputString = "8BADF00D";
    QString badHexadecimalInputString = "6157eXy82A";
    QVERIFY(conv->Converter_Parse(hexadecimalInputString, HEXADECIMAL, BINARY) == "10001011101011011111000000001101");
    QVERIFY(conv->Converter_Parse(hexadecimalInputString, HEXADECIMAL, OCTAL) == "21353370015");
    QVERIFY(conv->Converter_Parse(hexadecimalInputString, HEXADECIMAL, DECIMAL) == "2343432205");
    QVERIFY(conv->Converter_Parse(hexadecimalInputString, HEXADECIMAL, HEXADECIMAL) == hexadecimalInputString);
    QVERIFY(conv->Converter_Parse(badHexadecimalInputString, HEXADECIMAL, HEXADECIMAL) == "Unsupported format.");
    delete conv;
}

void VerCalcTest::codeFirstGoodString()
{
    Encoder *code = new Encoder();
    QString goodString = "It is a nice day today.";
    QString codeOutput = code->Encoder_Parse(goodString);
    QVERIFY(code->Encoder_Decode(codeOutput) == "IT IS A NICE DAY TODAY.");
    delete code;
}

void VerCalcTest::codeSecondGoodString()
{
    Encoder *code = new Encoder();
    QString goodString = "Možná přijde i kouzelník.";
    QString codeOutput = code->Encoder_Parse(goodString);
    QVERIFY(code->Encoder_Decode(codeOutput) == "MOZNA PRIJDE I KOUZELNIK.");
    delete code;
}

void VerCalcTest::codeThirdGoodString()
{
    Encoder *code = new Encoder();
    QString goodString = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                         "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.";
    QString codeOutput = code->Encoder_Parse(goodString);
    QVERIFY(code->Encoder_Decode(codeOutput) == "LOREM IPSUM DOLOR SIT AMET, CONSECTETUER ADIPISCING ELIT. "
                                                "CUM SOCIIS NATOQUE PENATIBUS ET MAGNIS DIS PARTURIENT MONTES, NASCETUR RIDICULUS MUS.");
    delete code;
}


QTEST_MAIN(VerCalcTest)

#include "ver_calc_tests.moc"
