/**
 * @file        detail.cpp
 * @author      Tomas Bartosik
 * @date        18.12.2020
 * @brief       definition file for Encoder info dialog
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "detail.hpp"
#include "ui_detail.h"

/*Class definition: ---------------------------------------------------------*/
Detail::Detail(QWidget *parent) : QDialog(parent), detailUserInterface(new Ui::Detail)
{
    detailUserInterface->setupUi(this);
}

Detail::~Detail()
{
    delete detailUserInterface;
}

void Detail::Detail_ImportStatus(Encoder& coder)
{
    QList<QPair<unsigned int, QString>> rawEncodingInput = coder.Encoder_GetEncodingInput();
    QStringList encodingInput;
    for(auto const& item : rawEncodingInput)
    {
        encodingInput.append(QString::number(item.first));
    }

    QStringList encodingOutput = coder.Encoder_GetEncodingOutput().split("\n");
    encodingOutput.removeAll({});

    detailUserInterface->codeDetailTable = new QTableWidget;
    detailUserInterface->codeDetailTable->setColumnCount(2);
    detailUserInterface->codeDetailTable->setRowCount(encodingInput.count());
    detailUserInterface->codeDetailTable->setHorizontalHeaderLabels({tr("Conversion"), tr("Occurrences")});
    detailUserInterface->codeDetailTable->verticalHeader()->setVisible(false);

    for(int row = 0; row < encodingInput.count(); row++)
    {
        QTableWidgetItem *conversionElement = new QTableWidgetItem(encodingOutput.at(row));
        QTableWidgetItem *occurrenceElement = new QTableWidgetItem(encodingInput.at(row));
        detailUserInterface->codeDetailTable->setItem(row, 0, conversionElement);
        detailUserInterface->codeDetailTable->setItem(row, 1, occurrenceElement);
    }

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(detailUserInterface->codeDetailTable);
    detailUserInterface->frame->setLayout(layout);
}
