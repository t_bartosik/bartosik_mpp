/**
 * @file        calculator.cpp
 * @author      Tomas Bartosik
 * @date        14.12.2020
 * @brief       definition file for Calculator
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "calculator.hpp"

/*Class definition: ---------------------------------------------------------*/
Calculator::Calculator(){}

Calculator::~Calculator(){}

QString Calculator::Calculator_Calculate(QString first, QString second, QChar action)
{
    QVariant firstVar;
    QVariant secondVar;
    QString result;
    float firstValue = 0.0f;
    float secondValue = 0.0f;
    float resultValue = 0.0f;

    if(first.contains(','))
    {
        first.replace(',', '.');
    }
    if(second.contains(','))
    {
        second.replace(',', '.');
    }

    firstVar = QVariant(first);
    secondVar = QVariant(second);

    if(firstVar.canConvert<float>())
    {
        firstValue = firstVar.toFloat();
    }
    if(secondVar.canConvert<float>())
    {
        secondValue = secondVar.toFloat();
    }

    if(action == '+')
    {
        resultValue = firstValue + secondValue;
    }
    if(action == '-')
    {
        resultValue = firstValue - secondValue;
    }
    if(action == '*')
    {
        resultValue = firstValue * secondValue;
    }
    if(action == '/')
    {
        resultValue = firstValue / secondValue;
    }
    result = QString::number(resultValue);
    return result;
}

QString Calculator::Calculator_Parse(QString input)
{
    QString current = input;
    QString currentNumber;
    QString first;
    QString second;
    QStringList initialExpression;
    QStringList expression;
    QStringList::iterator s;
    QRegularExpression numbers("^\\d+$");
    QRegularExpression decimalPoints("[.,]");
    QRegularExpression operators("[+-/\\*]");

    current.remove(' ');

    if(input.length() < 1)
    {
        return QString();
    }

    initialExpression = current.split(QString());
    initialExpression.removeAll({});

    for(s = initialExpression.begin(); s < initialExpression.end(); s++)
    {
        if(s->contains(numbers) || s->contains(decimalPoints))
        {
            currentNumber.append(*s);
            if((s + 1) == initialExpression.end())
            {
                expression << currentNumber;
            }
        }
        else if(s->contains(operators))
        {
            expression << currentNumber;
            expression << *s;
            currentNumber = QString();
        }
        else
        {
            return tr("Unsupported format.");
        }
    }
    expression.removeAll({});

    if(expression.at(expression.size() - 1).contains(operators)
            && !(expression.at(expression.size() - 1).contains("."))
            && !(expression.at(expression.size() - 1).contains(",")))
    {
        return tr("Unsupported format.");
    }

    while(expression.size() > 2)
    {
        auto position = expression.indexOf("*", 1);
        if(position != -1)
        {
            first = expression.at(position - 1);
            second = expression.at(position + 1);

            expression.replace(position - 1, Calculator_Calculate(first, second, '*'));
            expression.removeAt(position);
            expression.removeAt(position);
            continue;
        }

        position = expression.indexOf("/", 1);
        if(position != -1)
        {
            first = expression.at(position - 1);
            second = expression.at(position + 1);

            expression.replace(position - 1, Calculator_Calculate(first, second, '/'));
            expression.removeAt(position);
            expression.removeAt(position);
            continue;
        }

        position = expression.indexOf("-", 1);
        if(position != -1)
        {
            first = expression.at(position - 1);
            if(position > 1)
            {
                if(expression.at(position - 2) == "-")
                {
                    first.prepend("-");
                    expression.removeAt(position - 2);
                    position--;
                }
            }
            second = expression.at(position + 1);
            currentNumber = Calculator_Calculate(first, second, '-');

            if(currentNumber.contains('-'))
            {
                currentNumber.remove('-');
                if(position > 1)
                {
                    if(expression.at(position - 2) == "+")
                    {
                        expression.replace(position - 2, "-");
                        expression.replace(position - 1, currentNumber);
                        expression.removeAt(position);
                        expression.removeAt(position);
                    }
                }
                else
                {
                    expression.replace(position - 1, "-");
                    expression.replace(position, currentNumber);
                    expression.removeAt(position + 1);
                }
            }
            else
            {
                expression.replace(position - 1, Calculator_Calculate(first, second, '-'));
                expression.removeAt(position);
                expression.removeAt(position);
            }
        }

        position = expression.indexOf("+", 1);
        if(position != -1)
        {
            first = expression.at(position - 1);
            second = expression.at(position + 1);
            if(position > 1)
            {
                if(expression.at(position - 2) == "-")
                {
                    first.prepend("-");
                    expression.removeAt(position - 2);
                    position--;
                }
            }

            expression.replace(position - 1, Calculator_Calculate(first, second, '+'));
            expression.removeAt(position);
            expression.removeAt(position);
        }
    }

    return expression.join("");
}

