/**
 * @file        converter.cpp
 * @author      Tomas Bartosik
 * @date        16.12.2020
 * @brief       definition file for class Converter
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "converter.hpp"

/*Class definition: ---------------------------------------------------------*/
Converter::Converter(){}

Converter::~Converter(){}

bool Converter::Converter_Check(QString input, int from)
{
    QRegularExpression condition;
    QStringList expression = input.split(QString());
    expression.removeAll({});

    if(from == BINARY)
    {
        condition.setPattern("[01]");
    }
    else if(from == OCTAL)
    {
        condition.setPattern("[0-7]");
    }
    else if(from == DECIMAL)
    {
        condition.setPattern("^\\d+$");
    }
    else if(from == HEXADECIMAL)
    {
        condition.setPattern("[0-9A-F]+");
    }

    for(auto const& c : expression)
    {
        if(!c.contains(condition))
        {
            return false;
        }
    }
    return true;
}

QString Converter::Converter_Convert(QString input, int from, int to)
{
    QVariant number;
    QStringList hexTable = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
    QStringList expression;
    QStringList::iterator index;
    QString output;
    unsigned int auxiliaryIndex = 1;
    unsigned long long auxiliaryNumber = 0;

    if(to != from)
    {
        expression = input.split(QString());
        expression.removeAll({});

        if(input == "0")
        {
            output = input;
        }
        else if(from == BINARY && to == OCTAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                if(*index == "1")
                {
                    auxiliaryNumber += pow(2, auxiliaryIndex - 1);
                }
                auxiliaryIndex++;
                if(auxiliaryIndex % 4 == 0)
                {
                    output.prepend(QString::number(auxiliaryNumber));
                    auxiliaryIndex = 1;
                    auxiliaryNumber = 0;
                }
            }
            if(auxiliaryIndex % 4 != 0 && auxiliaryNumber != 0)
            {
                output.prepend(QString::number(auxiliaryNumber));
            }
        }
        else if(from == BINARY && to == DECIMAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                if(*index == "1")
                {
                    auxiliaryNumber += pow(2, auxiliaryIndex - 1);
                }
                auxiliaryIndex++;
            }
            output = QString::number(auxiliaryNumber);
        }
        else if(from == BINARY && to == HEXADECIMAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                if(*index == "1")
                {
                    auxiliaryNumber += pow(2, auxiliaryIndex - 1);
                }
                auxiliaryIndex++;
                if(auxiliaryIndex % 5 == 0)
                {
                    output.prepend(hexTable.at(auxiliaryNumber));
                    auxiliaryIndex = 1;
                    auxiliaryNumber = 0;
                }
            }
            if(auxiliaryIndex % 5 != 0 && auxiliaryNumber != 0)
            {
                output.prepend(QString::number(auxiliaryNumber));
            }
        }
        else if(from == OCTAL && to == BINARY)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                number = *index;
                auxiliaryIndex = 3;
                while(number.toInt() > 0)
                {
                    auxiliaryNumber = number.toInt() % 2;
                    output.prepend(QString::number(auxiliaryNumber));
                    auxiliaryIndex--;
                    number = number.toInt() / 2;
                }
                if(index > expression.begin())
                {
                    for(unsigned int current = 1; current <= auxiliaryIndex; current++)
                    {
                        output.prepend("0");
                    }
                }
            }
        }
        else if(from == OCTAL && to == DECIMAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                number = *index;
                auxiliaryNumber += number.toInt() * pow(8, auxiliaryIndex - 1);
                auxiliaryIndex++;
            }
            output = QString::number(auxiliaryNumber);
        }
        else if(from == OCTAL && to == HEXADECIMAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                number = *index;
                auxiliaryNumber += number.toInt() * pow(8, auxiliaryIndex - 1);
                auxiliaryIndex++;
            }
            number = auxiliaryNumber;
            while(number.toULongLong() > 0)
            {
                auxiliaryNumber = number.toULongLong() % 16;
                output.prepend(hexTable.at(auxiliaryNumber));
                number = number.toULongLong() / 16;
            }
        }
        else if(from == DECIMAL && to == BINARY)
        {
            number = expression.join("");
            while(number.toULongLong() > 0)
            {
                auxiliaryNumber = number.toULongLong() % 2;
                output.prepend(QString::number(auxiliaryNumber));
                number = number.toULongLong() / 2;
            }
        }
        else if(from == DECIMAL && to == OCTAL)
        {
            number = expression.join("");
            while(number.toULongLong() > 0)
            {
                auxiliaryNumber = number.toULongLong() % 8;
                output.prepend(QString::number(auxiliaryNumber));
                number = number.toULongLong() / 8;
            }
        }
        else if(from == DECIMAL && to == HEXADECIMAL)
        {
            number = expression.join("");
            while(number.toULongLong() > 0)
            {
                auxiliaryNumber = number.toULongLong() % 16;
                output.prepend(hexTable.at(auxiliaryNumber));
                number = number.toULongLong() / 16;
            }
        }
        else if(from == HEXADECIMAL && to == BINARY)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                number = hexTable.indexOf(*index);
                auxiliaryIndex = 4;
                while(number.toInt() > 0)
                {
                    auxiliaryNumber = number.toInt() % 2;
                    output.prepend(QString::number(auxiliaryNumber));
                    auxiliaryIndex--;
                    number = number.toInt() / 2;
                }
                if(index > expression.begin())
                {
                    for(unsigned int current = 1; current <= auxiliaryIndex; current++)
                    {
                        output.prepend("0");
                    }
                }
            }
        }
        else if(from == HEXADECIMAL && to == OCTAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                auxiliaryNumber += hexTable.indexOf(*index) * pow(16, auxiliaryIndex - 1);
                auxiliaryIndex++;
            }
            number = auxiliaryNumber;
            while(number.toULongLong() > 0)
            {
                auxiliaryNumber = number.toULongLong() % 8;
                output.prepend(QString::number(auxiliaryNumber));
                number = number.toULongLong() / 8;
            }
        }
        else if(from == HEXADECIMAL && to == DECIMAL)
        {
            for(index = expression.end() - 1; index >= expression.begin(); index--)
            {
                auxiliaryNumber += hexTable.indexOf(*index) * pow(16, auxiliaryIndex - 1);
                auxiliaryIndex++;
            }
            output = QString::number(auxiliaryNumber);
        }
    }
    else
    {
        output = input;
    }

    return output;
}

QString Converter::Converter_Parse(QString input, int from, int to)
{
    QString result;

    if(input == QString())
    {
        return QString();
    }
    input = input.toUpper();

    if(Converter_Check(input, from) == true)
    {
        result = Converter_Convert(input, from, to);
    }
    else
    {
        return tr("Unsupported format.");
    }
    return result;
}
