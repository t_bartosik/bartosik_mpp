/**
 * @file        converter.hpp
 * @author      Tomas Bartosik
 * @date        16.12.2020
 * @brief       declaration file for Converter
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private defines: ----------------------------------------------------------*/
#ifndef CONVERTER_HPP
#define CONVERTER_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QMainWindow>
#include <QDebug>
#include <QRegularExpression>
#include <cmath>

enum Conversions
{
    BINARY = 0,
    OCTAL = 1,
    DECIMAL = 2,
    HEXADECIMAL = 3
};

/*Class declaration: --------------------------------------------------------*/
class Converter: public QObject
{
    Q_OBJECT

public:
    Converter();
    ~Converter();

    /*!
    * @brief Converter_Check (bool) accepts the input string provided by
    * the user and verifies that it matches the pattern of a specific number
    * base (defined by the int parameter)
    * @param input (QString) string to be checked by regular expression
    * @param from (int) depicts the selected number base (binary, octal,
    * decimal, hexadecimal)
    * @return TRUE if input matches the pattern, FALSE otherwise
    */
    bool Converter_Check(QString input, int from);
    /*!
    * @brief Converter_Convert (QString) manages the processing of conversion
    * between different number bases, if the input string is valid
    * @param input (QString) represents the input number
    * @param from (int) the input base of a number
    * @param to (int) the ouput base of a number
    * @return string that equals the output nubmer base
    */
    QString Converter_Convert(QString input, int from, int to);
    /*!
    * @brief Converter_Parse (QString) accepts the input string defined
    * by the user and orders its conversion that is afterwards reported
    * back to the user
    * @param input (QString) the input string defined by the user
    * @param from (int) initial number base (which is used to convert from)
    * @param to (int) secondary number base selected by the user
    * @return processed output string that is displayed via user interface
    */
    QString Converter_Parse(QString input, int from, int to);
};

#endif // CONVERTER_HPP
