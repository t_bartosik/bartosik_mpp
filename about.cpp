/**
 * @file        about.cpp
 * @author      Tomas Bartosik
 * @date        20.12.2020
 * @brief       definition file for class About
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "about.hpp"
#include "ui_about.h"

/*Class definition: ---------------------------------------------------------*/
About::About(QWidget *parent) : QDialog(parent), aboutUserInterface(new Ui::About)
{
    aboutUserInterface->setupUi(this);

    /*Connects: -------------------------------------------------------------*/
    connect(aboutUserInterface->visitButton, &QPushButton::clicked, this, &About::About_VisitWebsite);
}

About::~About()
{
    delete aboutUserInterface;
}

void About::About_ProcessContent()
{
    QPixmap image;
    if(image.load(":/images/applogo.png"))
    {
        aboutUserInterface->aboutImage->setPixmap(image);
    }
    aboutUserInterface->appNameLabel->setText(QCoreApplication::applicationName());
    aboutUserInterface->appVersionLabel->setText("ver. " + QCoreApplication::applicationVersion());
}

void About::About_VisitWebsite()
{
    QUrl repository("https://bitbucket.org/t_bartosik/bartosik_mpp/src/master/");
    QDesktopServices::openUrl(repository);
}
