/**
 * @file        mainwindow.hpp
 * @author      Tomas Bartosik
 * @date        02.12.2020
 * @brief       declaration file for MainWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private defines: ----------------------------------------------------------*/
#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QTranslator>
#include "detail.hpp"
#include "calculator.hpp"
#include "converter.hpp"
#include "encoder.hpp"
#include "about.hpp"

/*Namespace declaration: ----------------------------------------------------*/
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    /*!
    * @brief MainWindow_InitializeUi (void) handles the initial setting
    * of user interface objects (visibility of elements)
    */
    void MainWindow_InitializeUi();

private slots:
    /*!
    * @brief MainWindow_EnableCzech (void) is a slot function that responds
    * to the activation of Czech language selection by retranslating the user
    * interface
    */
    void MainWindow_EnableCzech();
    /*!
    * @brief MainWindow_EnableEnglish (void) is a slot function that disables
    * the activation of Czech language and enables its English counterpart,
    * which is triggered by pressing the language selection button
    */
    void MainWindow_EnableEnglish();
    /*!
    * @brief MainWindow_EnableCalculator (void) is responsible for hiding the
    * Converter and Encoder modules before displaying the Calculator widget
    */
    void MainWindow_EnableCalculator();
    /*!
    * @brief MainWindow_EnableConverter (void) is a slot function that enables
    * the usage of Converter class elements by hiding the Calculator and Encoder
    * user interface objects
    */
    void MainWindow_EnableConverter();
    /*!
    * @brief MainWindow_EnableEncoder (void) is a slot member function that
    * responds to toggling the Encoder activation button by hiding the Calculator
    * and Converter modules
    */
    void MainWindow_EnableEncoder();
    /*!
    * @brief MainWindow_Calculate (void) manages the processing of user-defined
    * string of numbers and operators (+, -, *, /) before displaying its result
    */
    void MainWindow_Calculate();
    /*!
    * @brief MainWindow_Convert (void) handles the calling of Converter member
    * functions that process the input string of numbers meant for conversion
    * from one number base to another
    */
    void MainWindow_Convert();
    /*!
    * @brief MainWindow_Encode (void) is a slot function that calls the Encoder
    * member function in order to encode and decode user defined string
    * of characters through an interpretation of Shannon-Fano encoding method
    */
    void MainWindow_Encode();
    /*!
    * @brief MainWindow_LoadInput (void) handles the opening of a text file to
    * be parsed into input text form for Shannon-Fano encoding
    */
    void MainWindow_LoadInput();
    /*!
    * @brief MainWindow_SaveOutput() is a slot member function that handles
    * the saving of Shannon-Fano encoding output to a file defined by the user
    */
    void MainWindow_SaveOutput();
    /*!
    * @brief MainWindow_ShowDetails (void) handles the opening of a dialog
    * containing additional informations (occurrences and code matching)
    * obtained by performing Shannon-Fano encoding
    */
    void MainWindow_ShowDetails();
    /*!
    * @brief MainWindow_CaptureScreenshot (void) is used for creation
    * of a screenshot of a currently activated widget (Calculator, Converter
    * or Encoder) that can be saved to user defined destination as
    * a PNG image
    */
    void MainWindow_CaptureScreenshot();
    /*!
    * @brief MainWindow_ShowAbout (void) opens up a new dialog containing
    * the application logo, name, version and project repository location
    * (which can be accessed via default web browser)
    */
    void MainWindow_ShowAbout();

protected:
    QTranslator *mTranslator;

private:
    Ui::MainWindow *userInterface;
    Calculator *mCalculator;
    Converter *mConverter;
    Encoder *mEncoder;
};

#endif // MAINWINDOW_HPP
