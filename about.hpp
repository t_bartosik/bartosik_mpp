/**
 * @file        about.hpp
 * @author      Tomas Bartosik
 * @date        20.12.2020
 * @brief       header file for About class implementation
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private defines: ----------------------------------------------------------*/
#ifndef ABOUT_HPP
#define ABOUT_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QDialog>
#include <QUrl>
#include <QDesktopServices>

/*Namespace declaration: ----------------------------------------------------*/
QT_BEGIN_NAMESPACE
namespace Ui { class About; }
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = nullptr);
    ~About();

    /*!
    * @brief About_ProcessContent (void) is responsible for mapping application
    * informations and logo to user interface
    */
    void About_ProcessContent();

private slots:
    /*!
    * @brief About_VisitWebsite (void) is used in order to trigger the opening
    * of source code website location
    */
    void About_VisitWebsite();

private:
    Ui::About *aboutUserInterface;
};

#endif // ABOUT_HPP
