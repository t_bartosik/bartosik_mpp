/**
 * @file        encoder.cpp
 * @author      Tomas Bartosik
 * @date        17.12.2020
 * @brief       definition file of class Encoder
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "encoder.hpp"

/*Class definition: ---------------------------------------------------------*/
Encoder::Encoder()
{
    mPlainInput = QString();
}

Encoder::~Encoder(){}

void Encoder::Encoder_Encode(QList<QPair<unsigned int, QString>> mappedInput, bool isInitial)
{
    QList<QPair<unsigned int, QString>> temporaryMapping = mappedInput;
    QList<QPair<unsigned int, QString>> upperSide;
    QList<QPair<unsigned int, QString>> lowerSide;
    QList<QPair<unsigned int, QString>>::iterator index;
    unsigned int total = 0;
    unsigned int currentSum = 0;
    int currentIndex = 0;

    if(isInitial == true)
    {
        mEncodingOutput.clear();
        for(index = mappedInput.end() - 1; index >= mappedInput.begin(); index--)
        {
            mEncodingOutput.prepend(QPair<QString, QString>(QString(), index->second));
        }
    }

    for(index = temporaryMapping.end() - 1; index >= temporaryMapping.begin(); index--)
    {
        total += index->first;
    }
    total /= 2;

    for(index = temporaryMapping.end() - 1; index >= temporaryMapping.begin(); index--)
    {
        for(auto position = 0; position < mEncodingOutput.size(); position++)
        {
            if(mEncodingOutput.at(position).second == index->second)
            {
                currentIndex = position;
            }
        }

        currentSum += index->first;
        if(currentSum > total)
        {
            mEncodingOutput[currentIndex].first.append("1");
            upperSide.prepend(QPair<unsigned int, QString>(index->first, index->second));
        }
        else
        {
            mEncodingOutput[currentIndex].first.append("0");
            lowerSide.prepend(QPair<unsigned int, QString>(index->first, index->second));
        }
    }

    if(upperSide.count() > 1)
    {
        Encoder_Encode(upperSide, false);
    }
    if(lowerSide.count() > 1)
    {
        Encoder_Encode(lowerSide, false);
    }
}

QString Encoder::Encoder_Decode(QString input)
{
    QString auxiliary;
    QString output;

    for(auto const& character : input)
    {
        auxiliary.append(character);
        for(auto position = 0; position < mEncodingOutput.size(); position++)
        {
            if(mEncodingOutput.at(position).first == auxiliary)
            {
                output.append(mEncodingOutput.at(position).second);
                auxiliary = QString();
            }
        }
    }

    return output;
}

QList<QPair<unsigned int, QString>> Encoder::Encoder_Mapping(QString input)
{
    QStringList expression = input.split("");
    QStringList characters;
    QList<QPair<unsigned int, QString>> mapping;

    expression.removeAll({});
    characters = expression;
    characters.removeDuplicates();

    for(auto const& ch : characters)
    {
        mapping.append(qMakePair(expression.filter(ch).count(), ch));
    }
    std::sort(mapping.begin(), mapping.end(), std::greater<QPair<unsigned int, QString>>());

    return mapping;
}

QString Encoder::Encoder_Parse(QString input)
{
    QRegularExpression characters("[^a-zA-Z0-9,.?!\\s]");
    QString current = input.toUpper();
    QString output;
    QString selectedCode;

    current = current.normalized(QString::NormalizationForm_D);
    mPlainInput = current.replace(characters, "");

    if(mPlainInput.length() > 0)
    {
        mEncodingInput = Encoder_Mapping(current);
        Encoder_Encode(mEncodingInput, true);

        for(auto const& character : mPlainInput)
        {
            for(auto position = 0; position < mEncodingOutput.size(); position++)
            {
                if(mEncodingOutput.at(position).second == character)
                {
                    output.append(mEncodingOutput.at(position).first);
                }
            }
        }

        return output;
    }

    return tr("Unsupported format.");
}

QString Encoder::Encoder_GetPlainInput()
{
    return mPlainInput;
}

QList<QPair<unsigned int, QString>> Encoder::Encoder_GetEncodingInput()
{
    return mEncodingInput;
}

QString Encoder::Encoder_GetEncodingOutput()
{
    QString output = "\n";
    for(auto const& character : mEncodingOutput)
    {
        if(character.second == " ")
        {
            output.append("   ' '  ->>  " + character.first + "\n");
        }
        else if(character.second == "\n")
        {
            output.append("   '\\n'  ->>  " + character.first + "\n");
        }
        else
        {
            output.append("   " + character.second + "  ->>  " + character.first + "\n");
        }
    }
    return output;
}
