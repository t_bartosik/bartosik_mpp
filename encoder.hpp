/**
 * @file        encoder.hpp
 * @author      Tomas Bartosik
 * @date        17.12.2020
 * @brief       header file of class Encoder
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private defines: ----------------------------------------------------------*/
#ifndef ENCODER_HPP
#define ENCODER_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QMainWindow>
#include <QDebug>
#include <QRegularExpression>

/*Class declaration: --------------------------------------------------------*/
class Encoder : public QObject
{
    Q_OBJECT

    public:
        Encoder();
        ~Encoder();

        /*!
        * @brief Encoder_Encode (void) recursively manages the processing of Shannon-Fano
        * encoding by splitting the input character groups in half and assigning appropriate
        * code, which is then stored inside the member property (mEncodingOutput)
        * @param mappedInput (QList<QPair<unsigned int, QString>>) is a list of
        * pairs (key-value) involving the occurrence of a character and its conversion
        * @param isInitial (bool) enables the processing only at the beginning of
        * the recursion
        */
        void Encoder_Encode(QList<QPair<unsigned int, QString>> mappedInput, bool isInitial);
        /*!
        * @brief Encoder_Decode (QString) provides the decoding of previously encoded string
        * which is possible through the table of key-value pairs (code matching the original
        * characters of the input string)
        * @param input (QString) containing the encoded sequence of characters
        * @return decoded string of characters (=> matching the input string
        * provided by the user)
        */
        QString Encoder_Decode(QString input);
        /*!
        * @brief Encoder_Mapping (QList<QPair<unsigned int, QString>>) handles
        * the splitting of input string into key-value pairs that provide information
        * about character occurrence
        * @param input (QString) input string defined by the user
        * @return a list of key-value pairs representing occurrences and characters
        * contained in the input string
        */
        QList<QPair<unsigned int, QString>> Encoder_Mapping(QString input);
        /*!
        * @brief Encoder_Parse (QString) accepts the input string provided
        * by the user, makes it upper case, removes diacritics, splits it and
        * orders its mapping before returning the merged sequence of coded
        * characters to the user
        * @param input (QString) original string of characters provided
        * by the user
        * @return encoded string of characters (using 1s and 0s)
        */
        QString Encoder_Parse(QString input);
        /*!
        * @brief Encoder_GetPlainInput (QString) simply returns the
        * diacritics-stripped upper case input string that is stored
        * as a private member property
        * @return private member property mPlainInput
        */
        QString Encoder_GetPlainInput();
        /*!
        * @brief Encoder_GetEncodingInput (QList<QPair<unsigned int, QString>>)
        * returns the private member property (a getter function)
        * @return private member property mEncodingInput
        */
        QList<QPair<unsigned int, QString>> Encoder_GetEncodingInput();
        /*!
        * @brief Encoder_GetEncodingOutput (QString) returns the modified
        * sequence of stored key-value pairs of occurrences and codes that
        * can be easily displayed via user interface
        * @return merged summary of character occurrances and codes
        */
        QString Encoder_GetEncodingOutput();

    private:
        QString mPlainInput;
        QList<QPair<unsigned int, QString>> mEncodingInput;
        QList<QPair<QString, QString>> mEncodingOutput;
};

#endif // ENCODER_HPP
