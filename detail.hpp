/**
 * @file        detail.hpp
 * @author      Tomas Bartosik
 * @date        18.12.2020
 * @brief       header file for Encoder detail dialog
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private defines: ----------------------------------------------------------*/
#ifndef DETAIL_HPP
#define DETAIL_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QDialog>
#include "encoder.hpp"

/*Namespace declaration: ----------------------------------------------------*/
QT_BEGIN_NAMESPACE
namespace Ui { class Detail; }
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class Detail : public QDialog
{
    Q_OBJECT

public:
    explicit Detail(QWidget *parent = nullptr);
    ~Detail();

    /*!
    * @brief Detail_ImportStatus (void) handles the provided Encoder object
    * reference and displays its contents to the user via user interface
    * @param coder (Encoder) representing currently processed Shannon-Fano
    * encoding
    */
    void Detail_ImportStatus(Encoder& coder);

private:
    Ui::Detail *detailUserInterface;
};

#endif // DETAIL_HPP
