/**
 * @file        calculator.hpp
 * @author      Tomas Bartosik
 * @date        14.12.2020
 * @brief       declaration file for class Calculator
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private defines: ----------------------------------------------------------*/
#ifndef CALCULATOR_HPP
#define CALCULATOR_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QMainWindow>
#include <QDebug>
#include <QRegularExpression>

/*Class declaration: --------------------------------------------------------*/
class Calculator : public QObject
{
    Q_OBJECT

public:
    Calculator();
    ~Calculator();

    /*!
    * @brief Calculator_Calculate (QString) manages the processing of input
    * values by performing a mathematic operation (+, -, * or /)
    * @param first (QString) first argument of computation
    * @param second (QString) second argument of computation
    * @param action (QChar) mathematic operator (+, -, * or /)
    * @return output string matching the result of an operation
    */
    QString Calculator_Calculate(QString first, QString second, QChar action);
    /*!
    * @brief Calculator_Parse (QString) splits the input string into a list
    * of chacters that are verified by regular expression and then processed
    * by Calculator_Calculate member function
    * @param input (QString) represents the input string obtained from the user
    * @return completely processed output string that is displayed to the user
    */
    QString Calculator_Parse(QString input);
};


#endif // CALCULATOR_HPP
