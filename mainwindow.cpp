/**
 * @file        mainwindow.cpp
 * @author      Tomas Bartosik
 * @date        02.12.2020
 * @brief       definition file for MainWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QApplication>

/*Class definition: ---------------------------------------------------------*/
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), userInterface(new Ui::MainWindow),
    mCalculator(new Calculator), mConverter(new Converter), mEncoder(new Encoder)
{
    userInterface->setupUi(this);

    QSettings appSettings;
    if(appSettings.value("general/language", "cz") == "cz")
    {
        userInterface->actionCzech->setChecked(true);
    }
    if(appSettings.value("general/language") == "en")
    {
        userInterface->actionEnglish->setChecked(true);
    }

    mTranslator = new QTranslator();
    mTranslator->load(":/languages/translation_" + appSettings.value("general/language", "cz").toString() + ".qm");
    qApp->installTranslator(mTranslator);
    userInterface->retranslateUi(this);

    MainWindow_InitializeUi();

    /*Connects: -------------------------------------------------------------*/
    connect(userInterface->actionCzech, &QAction::triggered, this, &MainWindow::MainWindow_EnableCzech);
    connect(userInterface->actionEnglish, &QAction::triggered, this, &MainWindow::MainWindow_EnableEnglish);

    connect(userInterface->actionCalculator, &QAction::triggered, this, &MainWindow::MainWindow_EnableCalculator);
    connect(userInterface->actionConverter, &QAction::triggered, this, &MainWindow::MainWindow_EnableConverter);
    connect(userInterface->actionEncoder, &QAction::triggered, this, &MainWindow::MainWindow_EnableEncoder);

    connect(userInterface->calculateButton, &QPushButton::clicked, this, &MainWindow::MainWindow_Calculate);

    connect(userInterface->convInputLine, &QLineEdit::textChanged, this, &MainWindow::MainWindow_Convert);
    connect(userInterface->convInCombo, &QComboBox::currentTextChanged, this, &MainWindow::MainWindow_Convert);
    connect(userInterface->convOutCombo, &QComboBox::currentTextChanged, this, &MainWindow::MainWindow_Convert);

    connect(userInterface->encodeButton, &QPushButton::clicked, this, &MainWindow::MainWindow_Encode);
    connect(userInterface->codeLoadButton, &QPushButton::clicked, this, &MainWindow::MainWindow_LoadInput);
    connect(userInterface->codeSaveButton, &QPushButton::clicked, this, &MainWindow::MainWindow_SaveOutput);
    connect(userInterface->codeDetailButton, &QPushButton::clicked, this, &MainWindow::MainWindow_ShowDetails);

    connect(userInterface->actionCapture_screenshot, &QAction::triggered, this, &MainWindow::MainWindow_CaptureScreenshot);
    connect(userInterface->actionAbout, &QAction::triggered, this, &MainWindow::MainWindow_ShowAbout);
}

MainWindow::~MainWindow()
{
    delete userInterface;
    delete mCalculator;
    delete mConverter;
    delete mEncoder;
}

void MainWindow::MainWindow_InitializeUi()
{
    userInterface->actionCalculator->setChecked(true);
    userInterface->conv_widget->hide();
    userInterface->code_widget->hide();

    userInterface->calcHistoryShow->toggle();
    userInterface->convHistoryShow->toggle();
    userInterface->codeSaveButton->setEnabled(false);
    userInterface->codeDetailButton->setEnabled(false);
}

void MainWindow::MainWindow_EnableCzech()
{
    QSettings appSettings;
    appSettings.setValue("general/language", "cz");

    qApp->removeTranslator(mTranslator);
    mTranslator->load(":/languages/translation_cz.qm");
    qApp->installTranslator(mTranslator);
    userInterface->retranslateUi(this);

    userInterface->actionEnglish->setChecked(false);
}

void MainWindow::MainWindow_EnableEnglish()
{
    QSettings appSettings;
    appSettings.setValue("general/language", "en");

    qApp->removeTranslator(mTranslator);
    mTranslator->load(":/languages/translation_en.qm");
    qApp->installTranslator(mTranslator);
    userInterface->retranslateUi(this);

    userInterface->actionCzech->setChecked(false);
}

void MainWindow::MainWindow_EnableCalculator()
{
    userInterface->conv_widget->hide();
    userInterface->code_widget->hide();
    userInterface->calc_widget->show();

    userInterface->actionConverter->setChecked(false);
    userInterface->actionEncoder->setChecked(false);
    userInterface->actionCalculator->setChecked(true);
}

void MainWindow::MainWindow_EnableConverter()
{
    userInterface->calc_widget->hide();
    userInterface->code_widget->hide();
    userInterface->conv_widget->show();

    userInterface->actionCalculator->setChecked(false);
    userInterface->actionEncoder->setChecked(false);
    userInterface->actionConverter->setChecked(true);
}

void MainWindow::MainWindow_EnableEncoder()
{
    userInterface->calc_widget->hide();
    userInterface->conv_widget->hide();
    userInterface->code_widget->show();

    userInterface->actionCalculator->setChecked(false);
    userInterface->actionConverter->setChecked(false);
    userInterface->actionEncoder->setChecked(true);
}

void MainWindow::MainWindow_Calculate()
{
    QString calcInput = userInterface->calcInputLine->text();
    QString calcOutput = mCalculator->Calculator_Parse(calcInput);
    QString calcHistory = calcInput + " = " + calcOutput;
    userInterface->calcOutputLine->setText(calcOutput);
    if(calcInput.length() > 0)
    {
         userInterface->calcHistoryOutput->append(calcHistory);
    }
}

void MainWindow::MainWindow_Convert()
{
    QString convInput = userInterface->convInputLine->text();
    int convFrom = userInterface->convInCombo->currentIndex();
    int convTo = userInterface->convOutCombo->currentIndex();
    QString convFromLabel = userInterface->convInCombo->currentText().toLower();
    QString convToLabel = userInterface->convOutCombo->currentText().toLower();
    QString convOutput = mConverter->Converter_Parse(convInput, convFrom, convTo);
    QString convHistory = convInput + " [" + convFromLabel + "] " + "  ->>  " + convOutput + " [" + convToLabel + "]";
    userInterface->convOutputLine->setText(convOutput);
    if(convInput.length() > 0)
    {
        userInterface->convHistoryOutput->append(convHistory);
    }
}

void MainWindow::MainWindow_Encode()
{
    QString codeInput = userInterface->codeInputLine->toPlainText();
    QString codeOutput = mEncoder->Encoder_Parse(codeInput);
    QString encodingCodeOutput = QString("%1  =>  %2\n%3").arg(mEncoder->Encoder_GetPlainInput()).arg(codeOutput)
            .arg(mEncoder->Encoder_GetEncodingOutput());
    QString decodedOutput;
    QString decodingCodeOutput;
    if(codeInput.length() > 0)
    {
        userInterface->codeOutputLine->setText(encodingCodeOutput);

        decodedOutput = mEncoder->Encoder_Decode(codeOutput);
        decodingCodeOutput = QString("%1  =>  %2\n").arg(codeOutput).arg(decodedOutput);
        userInterface->codeOutputLine->append(decodingCodeOutput);
        userInterface->codeSaveButton->setEnabled(true);
        userInterface->codeDetailButton->setEnabled(true);
    }
    else
    {
        userInterface->codeOutputLine->setText(QString());
        userInterface->codeSaveButton->setEnabled(false);
        userInterface->codeDetailButton->setEnabled(false);
    }
}

void MainWindow::MainWindow_LoadInput()
{
    QString source = QFileDialog::getOpenFileName(this, tr("Open text file"), "", tr("Text file (*.txt)"));
    if(source.isEmpty())
    {
        QMessageBox::critical(this, tr("Loading error"), tr("File could not be opened."));
    }
    else
    {
        QFile sourceFile(source);
        if(sourceFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream input(&sourceFile);
            userInterface->codeInputLine->setText(input.readAll());
            sourceFile.close();
        }
    }
}

void MainWindow::MainWindow_SaveOutput()
{
    QString destination = QFileDialog::getSaveFileName(this, tr("Save text file"), tr("output.txt"), tr("Text file (*.txt)"));
    if(destination.isEmpty())
    {
        QMessageBox::critical(this, tr("Saving error"), tr("Unable to save file."));
    }
    else
    {
        QFile destinationFile(destination);
        if(destinationFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream output(&destinationFile);
            output << userInterface->codeOutputLine->toPlainText();
            destinationFile.close();
        }
    }
}

void MainWindow::MainWindow_ShowDetails()
{
    Detail detail;
    detail.setModal(true);
    detail.setWindowTitle(tr("Detail"));
    detail.setWindowIcon(QIcon(":/images/appicon.ico"));
    detail.Detail_ImportStatus(*mEncoder);
    detail.exec();
}

void MainWindow::MainWindow_CaptureScreenshot()
{
    QPixmap screenshot = this->grab();
    QString destination = QFileDialog::getSaveFileName(this, tr("Save screenshot"), tr("screenshot.png"), "Portable Network Graphics (*.png)");
    if(destination.isEmpty())
    {
        QMessageBox::critical(this, tr("Saving error"), tr("Unable to save file."));
    }
    else
    {
        screenshot.save(destination);
    }
}

void MainWindow::MainWindow_ShowAbout()
{
    About about;
    about.setModal(true);
    about.setWindowTitle(tr("About"));
    about.setWindowIcon(QIcon(":/images/appicon.ico"));
    about.About_ProcessContent();
    about.exec();
}
