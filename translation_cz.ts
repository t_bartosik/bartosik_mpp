<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="20"/>
        <source>Dialog</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="about.ui" line="85"/>
        <source>Versatile Calculator</source>
        <translation>Versatile Calculator</translation>
    </message>
    <message>
        <location filename="about.ui" line="146"/>
        <source>Visit repository</source>
        <translation>Navštívit repozitář</translation>
    </message>
    <message>
        <location filename="about.ui" line="153"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
</context>
<context>
    <name>Calculator</name>
    <message>
        <location filename="calculator.cpp" line="109"/>
        <location filename="calculator.cpp" line="118"/>
        <source>Unsupported format.</source>
        <translation>Nepodporovaný formát.</translation>
    </message>
</context>
<context>
    <name>Converter</name>
    <message>
        <location filename="converter.cpp" line="273"/>
        <source>Unsupported format.</source>
        <translation>Nepodporovaný formát.</translation>
    </message>
</context>
<context>
    <name>Detail</name>
    <message>
        <location filename="detail.ui" line="26"/>
        <source>Dialog</source>
        <translation>Detail kódování</translation>
    </message>
    <message>
        <location filename="detail.ui" line="82"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="detail.cpp" line="40"/>
        <source>Conversion</source>
        <translation>Převod</translation>
    </message>
    <message>
        <location filename="detail.cpp" line="40"/>
        <source>Occurrences</source>
        <translation>Počet výskytů</translation>
    </message>
</context>
<context>
    <name>Encoder</name>
    <message>
        <location filename="encoder.cpp" line="148"/>
        <source>Unsupported format.</source>
        <translation>Nepodporovaný formát.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation>Versatile Calculator</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="35"/>
        <source>Encode</source>
        <translation>Zakódovat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="51"/>
        <source>Load from file</source>
        <translation>Načíst vstup</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Save to file</source>
        <translation>Uložit výstup</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>Show details</source>
        <translation>Zobrazit detaily</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="116"/>
        <source>Calculate</source>
        <translation>Spočítat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="139"/>
        <location filename="mainwindow.ui" line="265"/>
        <source>Show history</source>
        <translation>Zobrazit historii</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="196"/>
        <location filename="mainwindow.ui" line="224"/>
        <source>Binary</source>
        <translation>Binární</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="mainwindow.ui" line="229"/>
        <source>Octal</source>
        <translation>Osmičková</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="mainwindow.ui" line="234"/>
        <source>Decimal</source>
        <translation>Desítková</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <location filename="mainwindow.ui" line="239"/>
        <source>Hexadecimal</source>
        <translation>Šestnáctková</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="mainwindow.ui" line="335"/>
        <source>Quit</source>
        <translation>Konec</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="mainwindow.ui" line="346"/>
        <source>Calculator</source>
        <translation>Kalkulačka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="354"/>
        <location filename="mainwindow.ui" line="357"/>
        <source>Converter</source>
        <translation>Převodník</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <location filename="mainwindow.ui" line="368"/>
        <source>Encoder</source>
        <translation>Kódování</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <source>Capture screenshot</source>
        <translation>Vytvořit snímek okna</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <location filename="mainwindow.cpp" line="260"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="386"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <source>Open text file</source>
        <translation>Otevřít textový soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Text file (*.txt)</source>
        <translation>Textový soubor (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <source>Loading error</source>
        <translation>Problém načítání</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <source>File could not be opened.</source>
        <translation>Soubor se nepodařilo otevřít.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Save text file</source>
        <translation>Uložit textový soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="215"/>
        <source>output.txt</source>
        <translation>vystup.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <location filename="mainwindow.cpp" line="248"/>
        <source>Saving error</source>
        <translation>Problém s ukládáním</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <location filename="mainwindow.cpp" line="248"/>
        <source>Unable to save file.</source>
        <translation>Soubor se nepodařilo uložit.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Detail</source>
        <translation>Detail kódování</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="245"/>
        <source>Save screenshot</source>
        <translation>Uložit snímek obrazovky</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="245"/>
        <source>screenshot.png</source>
        <translation>snimek.png</translation>
    </message>
</context>
</TS>
