/**
 * @file        main.cpp
 * @author      Tomas Bartosik
 * @date        02.12.2020
 * @brief       main application file for VersatileCalculator
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2020 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "mainwindow.hpp"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("UTB");
    QCoreApplication::setOrganizationDomain("utb.cz");
    QCoreApplication::setApplicationName("Versatile Calculator");
    QCoreApplication::setApplicationVersion("1.0");

    MainWindow window;
    window.setWindowTitle(QCoreApplication::applicationName());
    window.setWindowIcon(QIcon(":/images/appicon.ico"));
    window.show();

    return app.exec();
}
